# jitsi-proxy

Some services such as Matrix element create a Jitsi call for videoconferencing. This is a PoC for launching another conferencing service.  

## Name

Jitsi calls redirection 

## Description

A proxy that takes a jitsi call request from any service and opens a conference in another system such as BigBlueButton or Zoom. This is intended for e.g. Matrix Element client. 

## How it works

At fairkom we did some preliminary tests and we do have an idea how it could work, but did not find a sponsor yet to continue the work. 

TO DO 

- more deeply analyse nginx call and hand over call to another service
- how do we hand over user name


## Installation

## Usage


## Support

If you want to sponsor the work, please contact support AT fairkom.eu

## Roadmap

tbd

## Contributing

- describe your use case as an issue
- tests tbd

## Authors and acknowledgment

tbd

## License

AGPL

## Project status

Concept phase. 
